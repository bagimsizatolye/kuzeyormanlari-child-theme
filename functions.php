<?php

function child_load_css() {
    wp_enqueue_style('style', get_stylesheet_uri() );
    wp_enqueue_style('Extra', get_template_directory_uri() . '/style.css' );
}

add_action('wp_enqueue_scripts', 'child_load_css');

add_action( 'after_setup_theme', 'load_childtheme_languages', 5 );
function load_childtheme_languages() {
	/* this theme supports localization */
    load_child_theme_textdomain( 'extra', get_stylesheet_directory() . '/languages' );
}
